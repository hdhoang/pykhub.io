---
layout: page
title: About
---

Hi there! My name is Bayu Aldi Yansyah and currently I'm studying Mathematics at [Airlangga University](http://unair.ac.id).

I'm obsessed with math (obviously), Data Science and Machine Learning. 

I believe that the best approach is to focus on the end goal and workflow, rather than the underlying technologies. I will use the best technology available to solve the problem.

My experiences include:

* **Jr. Data Scientist** at Sale Stock Indonesia (February 2016 - now)

* **Lead Software Engineer** at Sayoeti Artificial Intelligence (December 2015 - February 2016)

* **Lead Software Engineer** at Relieve - The Peace of Mind Social Network (January 2015 – September 2015)

See what's currently I'm [hacking on](https://github.com/pyk), what's I'm [talking about](https://twitter.com/peeyek) or review my [linkedin](https://www.linkedin.com/in/bayualdiyansyah) profile.
